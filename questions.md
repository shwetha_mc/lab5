# Questions for Lab 5

## Part 0: Getting started

* *What is the name of the processor which you used for this assignment?*
Intel(R) Xeon(R) CPU           X5650  @ 2.67GHz

Part 1: Profiling
---------------------

* *What is the IPC of the utility in our use case? Is it good?*
0.93. It has scope for parallelization. Any number less than 2 can be efficiently parallelized.

* *What is the fraction of mispredicted branches? Is it acceptable?*
0.13% of all branches are mispredicted. This is nearly around 1.6 Million incorrectly predicted branches. This is a very good number because branch rework needs to be performed only on the small fraction of 0.13% of the total branches, although the branch misprediction delay would be around 20-25 clock cycles per misprediction, the correct predictions save us one instruction per cycle. So for every 100 branch predictions, 3.2 clock cycles are wasted because of mispredictions and 99.87 clock cycles are saved.  That is a net save of 96.67 clock cycles per 100 branch predictions.  

* *What is the rate of cache misses? How do you feel about this number?*
0.001M/sec, or 3475 page faults on an average.

* *Which two functions take most of the execution time? What do they do?*
MorphologyApply and memcpy.
In MorphologyApply, Add functions seem to be taking up most of the time.
This function allows any changes to be applied to the image. On a very high level, this function allows the work done by the blur and colorspace utilities to actually be reflected in the output image.

Part 2: Compiler Optimizations
------------------------------

* *What is the "User Time" for program execution before you start optimizing?*
5.81 seconds

* *What is the "User Time" for program execution after you completed **all** three steps and rune the program with `-fprofile-use`*?
4.351 seconds
