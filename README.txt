CC = icc
CFLAGS = -ipo -O3 -prof-gen -march=corei7 -no-inline-calloc -ipp
LDFLAGS = -ipo -O3 -prof-gen -march=corei7 -no-inline-calloc -ipp
Alongwith AR=xiar for ipo compatibility

ipo - inter procedural optimization. Postpones code generation till link time.
prof-gen - for profile generation. After a couple of runs, .dyn files would be generated, which would then be used for the next run with -prof-use.
-march - architecture specific full utilization. 
Note - xHost performs optimizations particular to a specific instruction set, but the code wouldn't compile after a certain run.
*Edit* - xHost runs without compilation errors. 
Initial run recorded with xhost and ipo only time - 8.8 seconds
-no-inline-calloc - inlines calls to malloc. Helpful while handling large data.
-ipp - intel performance package libraries are specially optimised to run on Intel processors and contain specialized functionality for image processing.
-O3 - is specially used for floating point optimizations (the image arrays being used in the librray are double type).

Here are some of the sample readings that I took over the week:
no opt | 5.81

prof gen | 8.168

prof use | 4.351

-ip | 5.037
-ip -no-inline-alloc | 5.035
-ip -ipp | 6.33
-ip -ipo-c 
-march and mtune |6.3/6.6
-ip -O3
-ip Os | 8.3
--ip xHost | 8.8
-Os | 5.7
-Os profgen |	8.8 / 9.2	
-Os prof use | 4.6
-ip -Os -prof-use | 4.5
-ip -Os -prof-use -march | 4.5
-ip -Os -prof-use -march -ipp | 4.538 on jinx-12 | 3.594
-ip -Os -prof-use -march -ipp xHost noinline calloc |
-fast -march -profuse -ipp |
-ipo -Of march prof gen | 8.1sec
same as above -O3 | 7.49
